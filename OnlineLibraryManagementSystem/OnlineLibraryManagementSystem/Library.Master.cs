﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem
{
    public partial class Library : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userType = (string)Session["User"];
            if (Session["User"] == null)
            {
                Response.Redirect("UserLogin.aspx");
            }
            if (userType == "Student")
            {
                panelStudent.Visible = true;
                panelAdmin.Visible = false;
            }
            if (userType == "Admin")
            {
                panelStudent.Visible = false;
                panelAdmin.Visible = true;
            }



        }

        protected void btnStudentLogOut_Click(object sender, EventArgs e)
        {
            Session["User"] = null;
            Session["StudentLogin"] = null;
            Session["StudentColumnId"] = null;
            Session["StudentId"] = null;
            Response.Redirect("UserLogin.aspx");
        }

        protected void btnAdminLogOut_Click(object sender, EventArgs e)
        {
            Session["User"] = null;
            Response.Redirect("UserLogin.aspx");
        }
    }
}