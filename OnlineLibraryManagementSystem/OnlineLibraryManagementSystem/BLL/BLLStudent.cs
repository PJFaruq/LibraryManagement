﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibraryManagementSystem.BLL
{
    public class BLLStudent
    {
        public int id { get; set; }
        public string StudentId { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Gender{ get; set; }
        public string Dob{ get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}