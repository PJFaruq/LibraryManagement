﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibraryManagementSystem.BLL
{
    public class BllBook
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public string Author { get; set; }
        public string Publication { get; set; }
        public string Category { get; set; }
        public int Available { get; set; }
        public int Rent { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string ImageUrl { get; set; }
        

    }
}