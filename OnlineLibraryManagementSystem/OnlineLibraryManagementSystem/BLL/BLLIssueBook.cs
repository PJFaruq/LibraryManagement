﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibraryManagementSystem.BLL
{
    public class BLLIssueBook
    {
        public int id { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string IssueDate { get; set; }
        public string ReturnDate { get; set; }
        public int Days { get; set; }
    }
}