﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineLibraryManagementSystem.BLL;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace OnlineLibraryManagementSystem.DAL
{
    public class dalBook
    {
       private static readonly string cs = @"server=DESKTOP-N5TPFUQ\SQLEXPRESS; database=LibrarySystem; integrated security=true";
        //private static readonly string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        bool status = false;
        DataTable dt = new DataTable();
        internal bool AddPublication(BllBook book)
        {
            try
            {
                using(SqlConnection con=new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into Publication values(@publication)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@publication", book.Publication);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable DeleteBook(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "delete from Book where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id",id);
                        
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetBookDetail(string bookId)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Book where id=@bookId";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@bookId", bookId);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetBookAndAuthorName(string bookName, string authorName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Book where name=@bookName and author=@authorName" +
                        "";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@bookName", bookName);
                        cmd.Parameters.AddWithValue("@authorName", authorName);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetAllPublication()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Publication";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetAllCategory()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Category";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }

        internal bool GetLogin(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Login where userName=@username and password=@password";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        //cmd.Parameters.AddWithValue("@username", bllBook.UserName);
                        //cmd.Parameters.AddWithValue("@password", bllBook.Password);
                       
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal void UpdateAvailable(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "Update Book set available=@available,rent=@rent where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", bllBook.id);
                        cmd.Parameters.AddWithValue("@available", bllBook.Available);
                        cmd.Parameters.AddWithValue("@rent", bllBook.Rent);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        internal DataTable GetBook(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Book where name=@name or author=@author or publication=@publication or category=@category";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@name", bllBook.Name);
                        cmd.Parameters.AddWithValue("@author",bllBook.Author);
                        cmd.Parameters.AddWithValue("@publication", bllBook.Publication);
                        cmd.Parameters.AddWithValue("@category", bllBook.Category);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable CheckBook(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Book where name=@name and author=@author";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@name", bllBook.Name);
                        cmd.Parameters.AddWithValue("@author", bllBook.Author);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal bool EditBook(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "Update Book set name=@name,detail=@detail,author=@author,publication=@publication, category=@category,price=@price,quantity=@quantity,imageUrl=@imageUrl where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", bllBook.id);
                        cmd.Parameters.AddWithValue("@name", bllBook.Name);
                        cmd.Parameters.AddWithValue("@detail", bllBook.Detail);
                        cmd.Parameters.AddWithValue("@author", bllBook.Author);
                        cmd.Parameters.AddWithValue("@publication", bllBook.Publication);
                        cmd.Parameters.AddWithValue("@category", bllBook.Category);
                        cmd.Parameters.AddWithValue("@price", bllBook.Price);
                        cmd.Parameters.AddWithValue("@quantity", bllBook.Quantity);
                        cmd.Parameters.AddWithValue("@imageUrl", bllBook.ImageUrl);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal bool AddCategory(BllBook book)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into Category values(@category)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@category", book.Category);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal bool AddBook(BllBook bllBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into Book(name,detail,author,publication, category,price,quantity,available,rent,imageUrl) values(@name,@detail,@author,@publication, @category,@price,@quantity,@available,@rent,@imageUrl)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@name", bllBook.Name);
                        cmd.Parameters.AddWithValue("@detail", bllBook.Detail);
                        cmd.Parameters.AddWithValue("@author", bllBook.Author);
                        cmd.Parameters.AddWithValue("@publication", bllBook.Publication);
                        cmd.Parameters.AddWithValue("@category", bllBook.Category);
                        cmd.Parameters.AddWithValue("@price", bllBook.Price);
                        cmd.Parameters.AddWithValue("@quantity", bllBook.Quantity);
                        cmd.Parameters.AddWithValue("@available", bllBook.Available);
                        cmd.Parameters.AddWithValue("@rent", bllBook.Rent);
                        cmd.Parameters.AddWithValue("@imageUrl", bllBook.ImageUrl);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable GetCategory(BllBook book)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Category where category= @Category";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@Category", book.Category);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }

        internal DataTable GetPublication(BllBook book)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Publication where publication= @publication";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@publication", book.Publication);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }
    }
}