﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineLibraryManagementSystem.BLL;
using System.Data;
using System.Data.SqlClient;

namespace OnlineLibraryManagementSystem.DAL
{
    public class DALIssueBook
    {
        private static readonly string cs = @"server=DESKTOP-N5TPFUQ\SQLEXPRESS; database=LibrarySystem; integrated security=true";
        //private static readonly string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        bool status = false;
        DataTable dt = new DataTable();
        internal bool InsertIssueBook(BLLIssueBook bllIssueBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into IssueBook(studentId,studentName,bookName,authorName, issueDate,returnDate,Days) values(@studentId,@studentName,@bookName,@authorName, @issueDate,@returnDate,@Days)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@studentId", bllIssueBook.StudentId);
                        cmd.Parameters.AddWithValue("@studentName",bllIssueBook.StudentName);
                        cmd.Parameters.AddWithValue("@bookName", bllIssueBook.BookName);
                        cmd.Parameters.AddWithValue("@authorName", bllIssueBook.AuthorName);
                        cmd.Parameters.AddWithValue("@issueDate", bllIssueBook.IssueDate);
                        cmd.Parameters.AddWithValue("@returnDate", bllIssueBook.ReturnDate);
                        cmd.Parameters.AddWithValue("@Days", bllIssueBook.Days);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal bool DeleteReturnBook(BLLIssueBook bllIssueBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "delete from IssueBook where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", bllIssueBook.id);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable GetReturnBookDetail(BLLIssueBook bllIssueBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from IssueBook where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", bllIssueBook.id);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetReturnBook(BLLIssueBook bllIssueBook)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from IssueBook where studentId=@studentId or bookName=@bookName";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@studentId", bllIssueBook.StudentId);
                        cmd.Parameters.AddWithValue("@bookName", bllIssueBook.BookName);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }
    }
}