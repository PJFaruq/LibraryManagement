﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineLibraryManagementSystem.BLL;
using System.Data;
using System.Data.SqlClient;

namespace OnlineLibraryManagementSystem.DAL
{
    public class DALStudent
    {
        private static readonly string cs = @"server=DESKTOP-N5TPFUQ\SQLEXPRESS; database=LibrarySystem; integrated security=true";
        bool status = false;
        DataTable dt = new DataTable();
        DataTable datatable = new DataTable();
        internal bool AddDepartment(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into Department values(@Department)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@Department", bllstudent.Department);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal bool DeleteStudent(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "delete from Student where id=@id ";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        int rowAffected = cmd.ExecuteNonQuery();

                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable GetAdminMailPassword(string email, string password)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Admin where email=@email and password=@password ";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@email",email);
                        cmd.Parameters.AddWithValue("@password", password);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(datatable);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return datatable;
        }

        internal DataTable dalGetStudentMailPassword(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Student where email=@email and password=@password ";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@email", bllstudent.Email);
                        cmd.Parameters.AddWithValue("@password", bllstudent.Password);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        int count = dt.Rows.Count;
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetStudentDetail(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Student where id=@id ";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", bllstudent.id);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetAllStudent()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Student ";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetStudent(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Student where studentId=@studentId or department=@department";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {

                        cmd.Parameters.AddWithValue("@studentId", bllstudent.StudentId);
                        cmd.Parameters.AddWithValue("@department", bllstudent.Department);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable GetAllDepartment()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Department";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable CheckDepartment(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Department where department=@department";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@department", bllstudent.Department);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal DataTable CheckAdmin(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Admin where email=@email";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@email", bllstudent.Email);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal bool AddStudents(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into Student values(@studentId,@name,@department,@gender,@dateOfBirth,@mobile,@address,@city,@imageUrl,@email,@password)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@studentId", bllstudent.StudentId);
                        cmd.Parameters.AddWithValue("@name", bllstudent.Name);
                        cmd.Parameters.AddWithValue("@department", bllstudent.Department);
                        cmd.Parameters.AddWithValue("@gender", bllstudent.Gender);
                        cmd.Parameters.AddWithValue("@dateOfBirth", bllstudent.Dob);
                        cmd.Parameters.AddWithValue("@mobile", bllstudent.Mobile);
                        cmd.Parameters.AddWithValue("@address", bllstudent.Address);
                        cmd.Parameters.AddWithValue("@city", bllstudent.City);
                        cmd.Parameters.AddWithValue("@imageUrl", bllstudent.ImageUrl);
                        cmd.Parameters.AddWithValue("@email", bllstudent.Email);
                        cmd.Parameters.AddWithValue("@password", bllstudent.Password);
                        int rowAffected = cmd.ExecuteNonQuery();

                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable CheckStudentId(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from Student where studentId=@studentId or email=@email";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@studentId", bllstudent.StudentId);
                        cmd.Parameters.AddWithValue("@email", bllstudent.Email);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal bool UpdateStudent(BLLStudent bllstudent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "update Student set studentId=@studentId,name=@name,department=@department,gender=@gender,dateOfBirth=@dateOfBirth,mobile=@mobile,address=@address,city=@city,imageUrl=@imageUrl,email=@email,password=@password where id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id",bllstudent.id);
                        cmd.Parameters.AddWithValue("@studentId", bllstudent.StudentId);
                        cmd.Parameters.AddWithValue("@name", bllstudent.Name);
                        cmd.Parameters.AddWithValue("@department", bllstudent.Department);
                        cmd.Parameters.AddWithValue("@gender", bllstudent.Gender);
                        cmd.Parameters.AddWithValue("@dateOfBirth", bllstudent.Dob);
                        cmd.Parameters.AddWithValue("@mobile", bllstudent.Mobile);
                        cmd.Parameters.AddWithValue("@address", bllstudent.Address);
                        cmd.Parameters.AddWithValue("@city", bllstudent.City);
                        cmd.Parameters.AddWithValue("@imageUrl", bllstudent.ImageUrl);
                        cmd.Parameters.AddWithValue("@email", bllstudent.Email);
                        cmd.Parameters.AddWithValue("@password", bllstudent.Password);
                        int rowAffected = cmd.ExecuteNonQuery();

                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }
    }
}