﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="IssueBookReport.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.IssueBookReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Book Issue Report</div>
        </div>
        <div class="panel-body">
            <asp:GridView ID="GridViewCategory" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" Width="100%">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:BoundField DataField="studentId" HeaderText="Student Id" SortExpression="studentId">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="studentName" HeaderText="Student Name" SortExpression="studentName">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="bookName" HeaderText="Book Name" SortExpression="bookName">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="authorName" HeaderText="Author Name" SortExpression="authorName">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="issueDate" HeaderText="Issued Date" SortExpression="issueDate">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="returnDate" HeaderText="Return Date" SortExpression="returnDate">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Days" HeaderText="Days" SortExpression="Days">
                    <HeaderStyle Font-Size="Large" />
                    <ItemStyle Font-Size="Medium" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
            </asp:GridView>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:LibrarySystemConnectionString %>' SelectCommand="SELECT * FROM [IssueBook]" DeleteCommand="DELETE FROM [IssueBook] WHERE [id] = @id" InsertCommand="INSERT INTO [IssueBook] ([studentId], [studentName], [bookName], [authorName], [issueDate], [returnDate], [Days]) VALUES (@studentId, @studentName, @bookName, @authorName, @issueDate, @returnDate, @Days)" UpdateCommand="UPDATE [IssueBook] SET [studentId] = @studentId, [studentName] = @studentName, [bookName] = @bookName, [authorName] = @authorName, [issueDate] = @issueDate, [returnDate] = @returnDate, [Days] = @Days WHERE [id] = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="studentId" Type="String" />
                    <asp:Parameter Name="studentName" Type="String" />
                    <asp:Parameter Name="bookName" Type="String" />
                    <asp:Parameter Name="authorName" Type="String" />
                    <asp:Parameter Name="issueDate" Type="String" />
                    <asp:Parameter Name="returnDate" Type="String" />
                    <asp:Parameter Name="Days" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="studentId" Type="String" />
                    <asp:Parameter Name="studentName" Type="String" />
                    <asp:Parameter Name="bookName" Type="String" />
                    <asp:Parameter Name="authorName" Type="String" />
                    <asp:Parameter Name="issueDate" Type="String" />
                    <asp:Parameter Name="returnDate" Type="String" />
                    <asp:Parameter Name="Days" Type="Int32" />
                    <asp:Parameter Name="id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
