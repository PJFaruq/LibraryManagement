﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="AddPublication.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.AddPublication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Add a Publication</div>
        </div>
        <div class="panel-body">
            <div style="padding-bottom:10px"> 
                <asp:Label ID="lblErrorMsg" runat="server" style="padding-left:250px"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Publication's Name</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtPublication" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success btn-block btn-lg" OnClick="btnAdd_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">All Publication</div>
        </div>
        <div class="panel-body">
            <asp:GridView ID="GridViewPublication" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" Width="517px">
                <AlternatingRowStyle BackColor="PaleGoldenrod"></AlternatingRowStyle>

                <Columns>
                    <asp:BoundField DataField="publication" HeaderText="Publication" SortExpression="publication">
                    <HeaderStyle Font-Size="X-Large" />
                    <ItemStyle Font-Size="Large" />
                    </asp:BoundField>
                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" HeaderText="Action">
                    <HeaderStyle Font-Size="X-Large" />
                    <ItemStyle Font-Size="Large" />
                    </asp:CommandField>
                </Columns>

                <FooterStyle BackColor="Tan"></FooterStyle>

                <HeaderStyle BackColor="Tan" Font-Bold="True"></HeaderStyle>

                <PagerStyle HorizontalAlign="Center" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"></PagerStyle>

                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite"></SelectedRowStyle>

                <SortedAscendingCellStyle BackColor="#FAFAE7"></SortedAscendingCellStyle>

                <SortedAscendingHeaderStyle BackColor="#DAC09E"></SortedAscendingHeaderStyle>

                <SortedDescendingCellStyle BackColor="#E1DB9C"></SortedDescendingCellStyle>

                <SortedDescendingHeaderStyle BackColor="#C2A47B"></SortedDescendingHeaderStyle>
            </asp:GridView>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:LibrarySystemConnectionString %>' DeleteCommand="DELETE FROM [Publication] WHERE [id] = @id" InsertCommand="INSERT INTO [Publication] ([publication]) VALUES (@publication)" SelectCommand="SELECT * FROM [Publication]" UpdateCommand="UPDATE [Publication] SET [publication] = @publication WHERE [id] = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32"></asp:Parameter>
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="publication" Type="String"></asp:Parameter>
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="publication" Type="String"></asp:Parameter>
                    <asp:Parameter Name="id" Type="Int32"></asp:Parameter>
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
