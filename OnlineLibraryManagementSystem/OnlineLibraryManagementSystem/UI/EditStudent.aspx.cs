﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class EditStudent : System.Web.UI.Page
    {
        string  name, path;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Calendar.Visible = false;
                GetDepartment();
                if (Request.QueryString["EditStudentId"] != null)
                {
                    string id = Request.QueryString["EditStudentId"];
                    UpdateStudent(id);
                }
                if (Request.QueryString["EditStudentId"] == null)
                {
                    Response.Redirect("ViewStudent.aspx");
                }
            }
            
        }

        private void UpdateStudent(string id)
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            bllstudent.id =Convert.ToInt32( id);
            dt = dalstudent.GetStudentDetail(bllstudent);

            txtStudentID.Text = dt.Rows[0]["studentId"].ToString();
            txtStudentName.Text= dt.Rows[0]["name"].ToString();
            ListItem department = new ListItem(dt.Rows[0]["department"].ToString());
            ddlDepartment.Items.Insert(0, department);
            rdbtnGender.SelectedValue = dt.Rows[0]["gender"].ToString();
            txtCalender.Text= dt.Rows[0]["dateOfBirth"].ToString();
            txtMobile.Text= dt.Rows[0]["mobile"].ToString();
            txtAddress.Text= dt.Rows[0]["address"].ToString();
            txtCity.Text= dt.Rows[0]["city"].ToString();
            txtEmail.Text= dt.Rows[0]["email"].ToString();
            txtPassword.Text= dt.Rows[0]["password"].ToString();
            Session["StudentImagePath"]= dt.Rows[0]["imageUrl"].ToString();

        }

        private void GetDepartment()
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            dt = dalstudent.GetAllDepartment();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
            ListItem category = new ListItem("Select One", "-1");
            ddlDepartment.Items.Insert(0, category);
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            if (Calendar.Visible == false)
            {
                Calendar.Visible = true;
            }
            else
            {
                Calendar.Visible = false;
            }
        }

        protected void btnEditStudent_Click(object sender, EventArgs e)
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            bool st = false;

            if (fileImage.HasFile)
            {
                name = fileImage.PostedFile.FileName;
                string extension = System.IO.Path.GetExtension(fileImage.FileName);
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                {

                    fileImage.SaveAs(Server.MapPath("~/Image/" + name));
                    path = "../Image/" + name.ToString();
                }
                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Only .jpg and .png file Supported";
                    return;
                }
            }
            if (!fileImage.HasFile)
            {
                path = (string)Session["StudentImagePath"];
            }
            if (txtStudentID.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter the Student Id";
                return;
            }
            if (txtStudentName.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter the Student Name";
                return;
            }
            if (ddlDepartment.SelectedItem.Text == "Select One")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Select a Department";
                return;
            }
            if (txtCalender.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Select the Date of Birth";
                return;
            }
            if (txtMobile.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Mobile Number";
                return;
            }
            if (txtAddress.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter the Address";
                return;
            }
            if (txtCity.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter the Name of City";
                return;
            }
            if (txtEmail.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Email";
                return;
            }
            if (txtPassword.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Password";
                return;
            }
            bllstudent.id =Convert.ToInt32(Request.QueryString["EditStudentId"]);
            bllstudent.StudentId = txtStudentID.Text;
            bllstudent.Name = txtStudentName.Text;
            bllstudent.Department = ddlDepartment.SelectedItem.Text;
            bllstudent.Gender = rdbtnGender.SelectedItem.Text;
            bllstudent.Dob = txtCalender.Text;
            bllstudent.Mobile = txtMobile.Text;
            bllstudent.Address = txtAddress.Text;
            bllstudent.City = txtCity.Text;
            bllstudent.ImageUrl = path;
            bllstudent.Email = txtEmail.Text;
            bllstudent.Password = txtPassword.Text;

            st = dalstudent.UpdateStudent(bllstudent);
            if (st == true)
            {
                Response.Redirect("ViewStudent.aspx");
            }
            else if (st == false)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Student Update Failed...";
            }

        }
        protected void Calendar_SelectionChanged(object sender, EventArgs e)
        {
            txtCalender.Text = Calendar.SelectedDate.ToShortDateString();
            Calendar.Visible = false;
        }
    }
}