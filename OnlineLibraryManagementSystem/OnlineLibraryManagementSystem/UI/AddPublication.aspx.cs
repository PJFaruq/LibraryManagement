﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class AddPublication : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            
            bool status = false;
            BllBook book = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            if (txtPublication.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Publication Name";
                return;
            }

            book.Publication = txtPublication.Text;
            dt = dalBook.GetPublication(book);
            if (dt.Rows.Count > 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                lblErrorMsg.Text = "This Publication Name is already Exist";
                return;
            }

            status = dalBook.AddPublication(book);
            if (status == true)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Publication Added Successfully........";
                txtPublication.Text = "";
                GridViewPublication.DataBind();
            }

        }
    }
}