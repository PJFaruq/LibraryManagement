﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class IssueBook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetddlCategory();
                GetddlPublication();
            }

        }

        private void GetddlPublication()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt1 = new DataTable();

            dt1 = dalBook.GetAllPublication();
            ddlPublication.DataSource = dt1;
            ddlPublication.DataBind();
            ListItem publication = new ListItem(" ", "-1");
            ddlPublication.Items.Insert(0, publication);
        }

        private void GetddlCategory()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalBook.GetAllCategory();
            ddlCategory.DataSource = dt;
            ddlCategory.DataBind();
            ListItem category = new ListItem(" ", "-1");
            ddlCategory.Items.Insert(0, category);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                BllBook bllBook = new BllBook();
                dalBook dalBook = new dalBook();
                DataTable dt = new DataTable();

                if (ddlCategory.SelectedItem.Text == " ")
                {
                    bllBook.Category = "";
                }
                if (ddlCategory.SelectedItem.Text != " ")
                {
                    bllBook.Category = ddlCategory.SelectedItem.Text;
                }
                if (ddlPublication.SelectedItem.Text == " ")
                {
                    bllBook.Publication = "";
                }
                if (ddlPublication.SelectedItem.Text != " ")
                {
                    bllBook.Publication = ddlPublication.SelectedItem.Text;
                }

                bllBook.Name = txtBookName.Text;
                bllBook.Author = txtBookName.Text;

                dt = dalBook.GetBook(bllBook);
                if (dt.Rows.Count <= 0)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "No Book is found";

                    rptrSearchBook.Visible = false;
                }
                else if (dt.Rows.Count > 0)
                {
                    rptrSearchBook.Visible = true;
                    lblErrorMsg.Visible = false;
                    rptrSearchBook.DataSource = dt;
                    rptrSearchBook.DataBind();
                }

            }
            catch
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Something Went Wrong";
            }
        }
    }
}