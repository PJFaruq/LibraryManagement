﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.AddCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Add a Category</div>
        </div>
        <div class="panel-body">
            <div style="padding-bottom:10px"> 
                <asp:Label ID="lblErrorMsg" runat="server" style="padding-left:250px"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Category Name</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success btn-block btn-lg" OnClick="btnAdd_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">All Category</div>
        </div>
        <div class="panel-body">
            <asp:GridView ID="GridViewCategory" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" Width="514px">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category">
                    <HeaderStyle Font-Size="X-Large" />
                    <ItemStyle Font-Italic="False" Font-Size="Large" />
                    </asp:BoundField>
                    <asp:CommandField HeaderText="Action" ShowDeleteButton="True" ShowEditButton="True">
                    <HeaderStyle Font-Size="X-Large" />
                    <ItemStyle Font-Size="Large" />
                    </asp:CommandField>
                </Columns>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
            </asp:GridView>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:LibrarySystemConnectionString %>' SelectCommand="SELECT * FROM [Category]" DeleteCommand="DELETE FROM [Category] WHERE [id] = @id" InsertCommand="INSERT INTO [Category] ([category]) VALUES (@category)" UpdateCommand="UPDATE [Category] SET [category] = @category WHERE [id] = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="category" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="category" Type="String" />
                    <asp:Parameter Name="id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
