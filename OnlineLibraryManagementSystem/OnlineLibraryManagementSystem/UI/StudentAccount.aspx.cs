﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class StudentAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["StudentLogin"] == null)
            {
                Response.Redirect("UserLogin.aspx");
            }
            if (Session["StudentLogin"] != null)
            {
                GetStudentDetail();
            }

               
        }
        private void GetStudentDetail()
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            bllstudent.id = Convert.ToInt32(Session["StudentColumnId"]);
            dt = dalstudent.GetStudentDetail(bllstudent);
            if (dt.Rows.Count > 0)
            {
                rptrStudentDetail.DataSource = dt;
                rptrStudentDetail.DataBind();
            }

        }
    }
}