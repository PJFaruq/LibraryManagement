﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class AddBook : System.Web.UI.Page
    {
        string name, path;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetddlCategory();
                GetddlPublication();
            }

            
       
         }

        private void GetddlPublication()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt1 = new DataTable();

            dt1 = dalBook.GetAllPublication();
            ddlPublication.DataSource = dt1;
            ddlPublication.DataBind();
            ListItem publication = new ListItem("Select One", "-1");
            ddlPublication.Items.Insert(0, publication);
        }

        private void GetddlCategory()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalBook.GetAllCategory();
            ddlCategory.DataSource = dt;
            ddlCategory.DataBind();
            ListItem category = new ListItem("Select One", "-1");
            ddlCategory.Items.Insert(0, category);
        }

        protected void btnAddBook_Click(object sender, EventArgs e)
        {

            bool status = false;
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            if (fileImage.HasFile)
            {
                name = fileImage.PostedFile.FileName;
                string extension = System.IO.Path.GetExtension(fileImage.FileName);
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                {

                    fileImage.SaveAs(Server.MapPath("~/Image/" + name));
                    path = "../Image/" + name.ToString();
                }
                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Only .jpg and .png file Supported";
                    return;
                }
            }
            if (!fileImage.HasFile)
            {
                path = "../Image/no_logo.png";
            }

            if (txtBookName.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Name";
                return;
            }
           if (txtDetail.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Detail";
                return;
            }
           if (txtAuthor.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Author Name";
                return;
            }
           

           if (ddlPublication.SelectedValue=="-1")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Select a Publication";
                return;
            }

           if (ddlCategory.SelectedValue == "-1")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Select a Category";
                return;
            }

          if (txtPrice.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Price";
                return;
            }

            double check;
           if (!double.TryParse(txtPrice.Text, out check))
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter Valid Book Price";
                return;
            }

          else  if (txtQuantity.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Quantity";
                return;
            }

            int check1;
           if (!int.TryParse(txtPrice.Text, out check1))
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter Valid Quantity";
                return;
            }
            

            bllBook.Name = txtBookName.Text;
            bllBook.Detail = txtDetail.Text;
            bllBook.Author = txtAuthor.Text;
            bllBook.Publication = ddlPublication.SelectedItem.Text;
            bllBook.Category = ddlCategory.SelectedItem.Text;
            bllBook.Price = Convert.ToDouble( txtPrice.Text);
            bllBook.Quantity = Convert.ToInt32(txtQuantity.Text);
            bllBook.Available = Convert.ToInt32(txtQuantity.Text);
            bllBook.Rent = 0;
            bllBook.ImageUrl = path;

            dt = dalBook.CheckBook(bllBook);

            if (dt.Rows.Count > 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "This Book is already Exist";
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                status = dalBook.AddBook(bllBook);

                if (status == true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Book Added Successfully....";
                    txtBookName.Text = "";
                    txtDetail.Text = "";
                    txtAuthor.Text = "";
                    txtPrice.Text = "";
                    txtQuantity.Text = "";
                }
                else if (status == false)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Book Added Failed";
                }
            }

            
        }
    }
}