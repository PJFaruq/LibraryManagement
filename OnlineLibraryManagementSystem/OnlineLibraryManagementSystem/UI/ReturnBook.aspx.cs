﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class ReturnBook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            BLLIssueBook bllIssueBook = new BLLIssueBook();
            DALIssueBook dalIssueBook = new DALIssueBook();
            DataTable dt = new DataTable();

            bllIssueBook.StudentId = txtStudentID.Text;
            bllIssueBook.BookName = txtBookName.Text;
            dt = dalIssueBook.GetReturnBook(bllIssueBook);

            if (dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "No Record is Found";
                rptrSearchBook.Visible = false;
                return;
            }
            if (dt.Rows.Count > 0)
            {
                lblErrorMsg.Visible = false;
                rptrSearchBook.Visible = true;
                rptrSearchBook.DataSource = dt;
                rptrSearchBook.DataBind();
            }
           

        }
    }
}