﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="EditBook.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.EditBook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Add a Book</div>
        </div>
        <div class="panel-body">
            <div style="padding-bottom:10px"> 
                <asp:Label ID="lblErrorMsg" runat="server" style="padding-left:250px"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Book Name:</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtEditBookName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Detail: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Author Name: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtAuthor" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Publication: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlPublication" CssClass="form-control" runat="server" DataTextField="publication" DataValueField="id"></asp:DropDownList>
                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Category: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlCategory" CssClass="form-control" runat="server" DataTextField="category" DataValueField="id"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Price: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Quantity: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Image: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="fileImage" runat="server" Height="36px" Width="257px" />
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <asp:Button ID="btnEditBook" runat="server" Text="Update" CssClass="btn btn-success btn-block btn-lg" OnClick="btnEditBook_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
