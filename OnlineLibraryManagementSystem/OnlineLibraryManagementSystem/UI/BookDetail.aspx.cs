﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class BookDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string bookId = (string)Request.QueryString["DetailId"];
            if (bookId == null)
            {
                Response.Redirect("SearchBook.aspx");
            }
            BookDetails();
        }

        private void BookDetails()
        {
            string bookId =(string) Request.QueryString["DetailId"];
            BllBook bllBook = new BllBook();
            dalBook dalbook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalbook.GetBookDetail(bookId);
            rptrBookDetail.DataSource = dt;
            rptrBookDetail.DataBind();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            
        }
    }
}