﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class EditBook : System.Web.UI.Page
    {
        string name, path;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetddlCategory();
                GetddlPublication();

                if (Request.QueryString["EditBookId"] != null)
                {
                    string id = Request.QueryString["EditBookId"];
                    UpdateBook(id);
                }
            }

            if (Request.QueryString["EditBookId"] == null)
            {
                Response.Redirect("SearchBook.aspx");
            }
        }

        private void GetddlPublication()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt1 = new DataTable();

            dt1 = dalBook.GetAllPublication();
            ddlPublication.DataSource = dt1;
            ddlPublication.DataBind();

        }

        private void GetddlCategory()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalBook.GetAllCategory();
            ddlCategory.DataSource = dt;
            ddlCategory.DataBind();
        }

        private void UpdateBook(string id)
        {
            bool status = false;
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();
            dt = dalBook.GetBookDetail(id);

            txtEditBookName.Text = dt.Rows[0]["name"].ToString();
            txtDetail.Text = dt.Rows[0]["detail"].ToString();
            txtAuthor.Text = dt.Rows[0]["author"].ToString();

            ListItem publication = new ListItem(dt.Rows[0]["publication"].ToString());
            ddlPublication.Items.Insert(0, publication);

            ListItem category = new ListItem(dt.Rows[0]["category"].ToString());
            ddlCategory.Items.Insert(0, category);

            txtPrice.Text = dt.Rows[0]["price"].ToString();
            txtQuantity.Text = dt.Rows[0]["quantity"].ToString();
            Session["BookImagePath"] = dt.Rows[0]["imageUrl"].ToString();

        }

        protected void btnEditBook_Click(object sender, EventArgs e)
        {
            bool status = false;
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            if (fileImage.HasFile)
            {
                name = fileImage.PostedFile.FileName;
                string extension = System.IO.Path.GetExtension(fileImage.FileName);
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                {

                    fileImage.SaveAs(Server.MapPath("~/Image/" + name));
                    path = "../Image/" + name.ToString();
                }
                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Only .jpg and .png file Supported";
                    return;
                }
            }
            if (!fileImage.HasFile)
            {
                path = (string)Session["BookImagePath"];
            }

            if (txtEditBookName.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Name";
                return;
            }
            if (txtDetail.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Detail";
                return;
            }
            if (txtAuthor.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Author Name";
                return;
            }

            if (txtPrice.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Price";
                return;
            }

            double check;
            if (!double.TryParse(txtPrice.Text, out check))
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter Valid Book Price";
                return;
            }

            if (txtQuantity.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Book Quantity";
                return;
            }

            int check1;
            if (!int.TryParse(txtPrice.Text, out check1))
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter Valid Quantity";
                return;
            }

            bllBook.id = Convert.ToInt32(Request.QueryString["EditBookId"]);
            bllBook.Name = txtEditBookName.Text;
            bllBook.Detail = txtDetail.Text;
            bllBook.Author = txtAuthor.Text;
            bllBook.Publication = ddlPublication.SelectedItem.Text;
            bllBook.Category = ddlCategory.SelectedItem.Text;
            bllBook.Price = Convert.ToDouble(txtPrice.Text);
            bllBook.Quantity = Convert.ToInt32(txtQuantity.Text);
            bllBook.ImageUrl = path;
            status = dalBook.EditBook(bllBook);

            if (status == true)
            {
                Response.Redirect("SearchBook.aspx?bdid=bookDetail");

            }
            else if (status == false)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Update Failed";
            }


        }
    }
}