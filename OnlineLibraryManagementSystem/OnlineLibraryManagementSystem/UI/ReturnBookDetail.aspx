﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="ReturnBookDetail.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.ReturnBookDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
            <div class="panel-heading">
            <div class="h3 text-center">
               Return Book Details
            </div>
        </div>
        <div class="panel-body">
                        <div style="padding-bottom:10px"> 
                <asp:Label ID="lblErrorMsg" runat="server" style="padding-left:250px"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <asp:Repeater ID="rptrReturnBookDetail" runat="server">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <table class="table-bordered table-responsive table table-condensed" style="font-size: large; font-weight: bold">
                                <tr>
                                    <td>Student Id: </td>
                                    <td>

                                        <%#Eval("studentId") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Student Name: </td>
                                    <td>

                                        <%#Eval("studentName") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Name: </td>
                                    <td>
                                        <%#Eval("bookName") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Author Name: </td>
                                    <td>
                                        <%#Eval("authorName") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Issue Date: </td>
                                    <td>
                                        <%#Eval("issueDate") %> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>Return Date: </td>
                                    <td>

                                        <%#Eval("returnDate") %>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Days: </td>
                                    <td>

                                        <%#Eval("Days") %>

                                    </td>
                                </tr>
                            </table>
                            </div>
                        </div>
                </ItemTemplate> 
            </asp:Repeater>
            <hr />

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <asp:Button ID="btnReturnBook" runat="server"  Text="Return Book" CssClass="btn btn-success btn-block btn-lg" OnClick="btnReturnBook_Click" />
                    </div>
                </div>
            </div>
            <a href="ReturnBook.aspx" class="btn btn-primary">Back</a>
        </div>
    </div>

</asp:Content>
