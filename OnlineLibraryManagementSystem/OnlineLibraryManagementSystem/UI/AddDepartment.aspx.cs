﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class AddDepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool status = false;
                BLLStudent bllstudent = new BLLStudent();
                DALStudent dalstudent = new DALStudent();
                DataTable dt = new DataTable();

                if (txtDepartment.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter Department Name";
                    return;
                }

                bllstudent.Department = txtDepartment.Text.ToUpper();

                dt = dalstudent.CheckDepartment(bllstudent);
                if (dt.Rows.Count > 0)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                    lblErrorMsg.Text = "This Department is already Exist......";
                    return;
                }

                status = dalstudent.AddDepartment(bllstudent);
                if (status == true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                    lblErrorMsg.Text = "Department Added Successfully........";
                    GridViewDepartment.DataBind();
                    txtDepartment.Text = "";

                }
            }
            catch
            {

            }
        }
    }
}