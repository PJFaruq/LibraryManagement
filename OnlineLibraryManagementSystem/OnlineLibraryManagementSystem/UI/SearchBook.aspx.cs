﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class SearchBook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if ((string)Request.QueryString["bdid"] != "bookDetail")
                {
                    Session["Publication"] = null;
                    Session["Category"] = null;
                    Session["Name"] = null;
                }

                GetddlCategory();
                GetddlPublication();
                if ((string)Request.QueryString["bdid"] == "bookDetail")
                {
                    LoadSearchResult();
                }
                     
            }

            if (IsPostBack)
            { string id = Request.QueryString["DelBookid"];
                if(id != null)
                {
                    DeleteBook(id);
                    
                    LoadSearchResult();
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Successfully Deleted";

                }
                
            }

        }

        private void DeleteBook(string id)
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalBook.DeleteBook(id);
            
        }

        private void LoadSearchResult()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();
            if ((string)Session["Name"]==null)
            {
                bllBook.Name = "";
                bllBook.Author = "";
            }
            if ((string)Session["Name"] != null)
            {
                bllBook.Name = (string)Session["Name"];
                bllBook.Author = (string)Session["Name"];
            }
            if ((string)Session["Category"] == null)
            {
                bllBook.Category = "";
            }
            if ((string)Session["Category"] != null)
            {
                bllBook.Category = (string)Session["Category"];
            }
            if ((string)Session["Publication"] == null)
            {
                bllBook.Publication = "";
            }
            if ((string)Session["Publication"] != null)
            {
                bllBook.Publication = (string)Session["Publication"];
            }
            
            dt = dalBook.GetBook(bllBook);
            if (dt.Rows.Count <= 0)
            {
                rptrSearchBook.Visible = false;
            }
            else if (dt.Rows.Count > 0)
            {
                rptrSearchBook.Visible = true;
                lblErrorMsg.Visible = false;
                rptrSearchBook.DataSource = dt;
                rptrSearchBook.DataBind();
            }

            
            
        }

        private void GetddlPublication()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt1 = new DataTable();

            dt1 = dalBook.GetAllPublication();
            ddlPublication.DataSource = dt1;
            ddlPublication.DataBind();
            ListItem publication = new ListItem("Select One", "-1");
            ddlPublication.Items.Insert(0, publication);
        }

        private void GetddlCategory()
        {
            BllBook bllBook = new BllBook();
            dalBook dalBook = new dalBook();
            DataTable dt = new DataTable();

            dt = dalBook.GetAllCategory();
            ddlCategory.DataSource = dt;
            ddlCategory.DataBind();
            ListItem category = new ListItem("Select One", "-1");
            ddlCategory.Items.Insert(0, category);
        }

        private void GetDropDownList()
        {
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                BllBook bllBook = new BllBook();
                dalBook dalBook = new dalBook();
                DataTable dt = new DataTable();

                if (ddlCategory.SelectedItem.Text=="Select One")
                {
                    bllBook.Category = "";
                    Session["Category"] = "";
                }
                if(ddlCategory.SelectedItem.Text != "Select One")
                {
                    bllBook.Category = ddlCategory.SelectedItem.Text;
                    Session["Category"]= ddlCategory.SelectedItem.Text;
                }
                if(ddlPublication.SelectedItem.Text== "Select One")
                {
                    bllBook.Publication = "";
                    Session["Publication"] = "";
                }
                if (ddlPublication.SelectedItem.Text != "Select One")
                {
                    bllBook.Publication = ddlPublication.SelectedItem.Text;
                    Session["Publication"]= ddlPublication.SelectedItem.Text;
                }
                if (txtBookName.Text == "")
                {
                    Session["Name"] = "";
                }

                bllBook.Name = txtBookName.Text;
                bllBook.Author = txtBookName.Text;
                Session["Name"]= txtBookName.Text;

                dt = dalBook.GetBook(bllBook);
                if (dt.Rows.Count <= 0)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "No Book is found";

                    rptrSearchBook.Visible = false;
                }
                else if(dt.Rows.Count>0)
                {
                    rptrSearchBook.Visible = true;
                    lblErrorMsg.Visible = false;
                    rptrSearchBook.DataSource = dt;
                    rptrSearchBook.DataBind();
                }
                
            }
            catch
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Something Went Wrong";
            }
        }

        
    }
}