﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="BookDetail.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.BookDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">
                Book Details
            </div>
        </div>
        <div class="panel-body">
            <asp:Repeater ID="rptrBookDetail" runat="server">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<%#Eval("imageUrl")%>" height="250" width="250" />
                        </div>
                        <div class="col-md-8">
                            <table class="table-bordered table-responsive table table-condensed" style="font-size: large; font-weight: bold">
                                <tr>
                                    <td>Book Name: </td>
                                    <td>

                                        <%#Eval("name") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Author Name: </td>
                                    <td>

                                        <%#Eval("author") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Category: </td>
                                    <td>
                                        <%#Eval("category") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Publication: </td>
                                    <td>
                                        <%#Eval("publication") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Price: </td>
                                    <td>
                                        <%#Eval("price") %> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Quantity: </td>
                                    <td>

                                        <%#Eval("quantity") %>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Available: </td>
                                    <td>

                                        <%#Eval("available") %>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Rent: </td>
                                    <td>

                                        <%#Eval("rent") %>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Detail: </td>
                                    <td>

                                        <%#Eval("detail") %>
                                    </td>
                                </tr>
                            </table>
                </ItemTemplate>
                
            </asp:Repeater>
            <a href="SearchBook.aspx?bdid=bookDetail" class="btn btn-success">Back</a>
        </div>
        
    </div>
   
</asp:Content>
