﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="ViewStudent.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.ViewStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <div class="h3 text-center">View Student</div>
        </div>

        <div class="panel-body">
            <div style="padding-bottom: 10px">
                <asp:Label ID="lblErrorMsg" runat="server" Style="padding-left: 250px" ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-horizontal">
                        <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Department: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" DataTextField="department" DataValueField="id"></asp:DropDownList>
                    </div>
                </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Student Id:</label>
                            </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtStudentId" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                            <div class="col-md-12">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success btn-block btn-lg" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                </div>
            </div>

            <div class="row" style="margin-top:15px">
                <div class="col-md-12">
                    <asp:Repeater ID="rptrSearchStudent" runat="server">
                        <HeaderTemplate>
                            <table class="table table-bordered table-responsive table-striped ">
                                <tr style="background-color: #d2b48c; font-size: large; font-weight: bold">
                                    <td>Student ID: </td>
                                    <td>Student Name:</td>
                                    <td>Department: </td>
                                    <td colspan="3">Actoin</td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Eval("studentId") %>
                                </td>
                                <td>
                                    <%#Eval("name") %>
                                </td>
                                <td>
                                    <%#Eval("department") %>
                                </td>

                                <td>
                                    <a href="StudentDetail.aspx?StudentDetailId=<%#Eval("id") %>">Details</a>
                                </td>
                                <td>
                                    <a href="EditStudent.aspx?EditStudentId=<%#Eval("id") %>">Edit</a>
                                </td>
                                <td>
                                    <a href="ViewStudent.aspx?DelStudentid=<%#Eval("id") %>">Delete</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>


        </div>
    </div>
</asp:Content>
