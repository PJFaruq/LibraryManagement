﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class IssueBookDetail : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            string bookId = (string)Request.QueryString["IssueBookId"];
            if (bookId == null)
            {
                Response.Redirect("IssueBook.aspx");
            }
            BookDetails();
        }
        private void BookDetails()
        {
            string bookId = (string)Request.QueryString["IssueBookId"];
            BllBook bllBook = new BllBook();
            dalBook dalbook = new dalBook();

            dt = dalbook.GetBookDetail(bookId);
            rptrBookDetail.DataSource = dt;
            rptrBookDetail.DataBind();
        }

        protected void btnIssueBook_Click(object sender, EventArgs e)
        {
            bool status = false;
            BLLIssueBook bllIssueBook = new BLLIssueBook();
            DALIssueBook dalIssueBook = new DALIssueBook();
            BllBook bllBook = new BllBook();
            dalBook dalbook = new dalBook();
            DALStudent dalstudent = new DALStudent();
            BLLStudent bllstudent = new BLLStudent();

            if (txtStudentID.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Student ID";
                return;
            }

            if (txtDays.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Days";
                return;
            }

            if (Convert.ToInt32(txtDays.Text) > 10)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Maximum 10 Days are Allowed";
                return;
            }

            if (Convert.ToInt32(txtDays.Text) <0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Please Enter Valid Amount of Days";
                return;
            }



            bllstudent.StudentId = txtStudentID.Text;
            bllstudent.Email = "";
           dt1= dalstudent.CheckStudentId(bllstudent);
            
            if (dt1.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "This Student Id is not Available";
                return;
            }
            if (Convert.ToInt32(dt.Rows[0]["available"]) <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "This Book is not Available";
                return;
            }

            bllIssueBook.StudentId= txtStudentID.Text;
            bllIssueBook.StudentName = dt1.Rows[0]["name"].ToString();
            bllIssueBook.BookName = dt.Rows[0]["name"].ToString();
            bllIssueBook.AuthorName = dt.Rows[0]["author"].ToString();
            bllIssueBook.IssueDate = DateTime.Now.ToShortDateString();

            DateTime issueDate = DateTime.Now;
            DateTime returnDate = issueDate.AddDays(Convert.ToDouble(txtDays.Text));
            bllIssueBook.ReturnDate = returnDate.ToShortDateString();
            bllIssueBook.Days =Convert.ToInt32( txtDays.Text);

            status = dalIssueBook.InsertIssueBook(bllIssueBook);
            if (status==true)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                lblErrorMsg.Text = "This Book is issued to " + dt1.Rows[0]["name"].ToString();

                int available =Convert.ToInt32( dt.Rows[0]["available"]);
                available--;
                int rent= Convert.ToInt32(dt.Rows[0]["rent"]);
                rent++;

                bllBook.id= Convert.ToInt32(dt.Rows[0]["id"].ToString());
                bllBook.Available = available;
                bllBook.Rent = rent;

                dalbook.UpdateAvailable(bllBook);
                txtStudentID.Text = "";
                txtDays.Text = "";
                BookDetails();
            }
        }
    }
}