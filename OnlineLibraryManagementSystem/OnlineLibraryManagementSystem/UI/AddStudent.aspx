﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="AddStudent.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.AddStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Add a Student</div>
        </div>
        <div class="panel-body">
            <div style="padding-bottom:10px"> 
                <asp:Label ID="lblErrorMsg" runat="server" style="padding-left:250px"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Student ID: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtStudentID" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Student Name: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtStudentName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Department: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" DataTextField="department" DataValueField="id"></asp:DropDownList>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Gender: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:RadioButtonList ID="rdbtnGender" style="padding-left:50px" runat="server" TextAlign="Right" RepeatDirection="Horizontal" Height="34px" Width="252px" CellPadding="1" CellSpacing="1">
                            <asp:ListItem  Text="Male" Selected="True" Value="Male"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Date of Birth: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:ImageButton ID="ImageButton1" CssClass="pull-right"  ImageUrl="~/Image/cal.png" Height="36" Width="45" BorderColor="Red" runat="server" OnClick="ImageButton1_Click" />
                        <asp:TextBox ID="txtCalender" runat="server" CssClass="form-control" Width="86%"></asp:TextBox>
                         
                        <asp:Calendar ID="Calendar" runat="server" OnSelectionChanged="Calendar_SelectionChanged"></asp:Calendar>
                        
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Mobile: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Address: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">City: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Image: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="fileImage" runat="server" Height="36px" Width="257px" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Email: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtEmail" TextMode="Email" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">Password: </label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <asp:Button ID="btnAddStudent" runat="server" Text="Add Student" CssClass="btn btn-success btn-block btn-lg" OnClick="btnAddStudent_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
