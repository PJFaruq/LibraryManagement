﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="StudentAccount.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.StudentAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">
                Student Details
            </div>
        </div>
        <div class="panel-body">
            <asp:Repeater ID="rptrStudentDetail" runat="server">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<%#Eval("imageUrl")%>" height="250" width="250" />
                        </div>
                        <div class="col-md-8">
                            <table class="table-bordered table-responsive table table-condensed" style="font-size: large; font-weight: bold">
                                <tr>
                                    <td>Student ID: </td>
                                    <td>

                                        <%#Eval("studentId") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Student Name: </td>
                                    <td>

                                        <%#Eval("name") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Department: </td>
                                    <td>
                                        <%#Eval("department") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gender: </td>
                                    <td>
                                        <%#Eval("gender") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date Of Birth: </td>
                                    <td>
                                        <%#Eval("dateOfBirth") %> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mobile: </td>
                                    <td>

                                        <%#Eval("mobile") %>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Address: </td>
                                    <td>

                                        <%#Eval("address") %>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>City: </td>
                                    <td>

                                        <%#Eval("city") %>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>Email: </td>
                                    <td>

                                        <%#Eval("email") %>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>Password: </td>
                                    <td>

                                        <%#Eval("password") %>
                                    </td>
                                </tr>
                                 
                            </table>
                </ItemTemplate>
                
            </asp:Repeater>
        </div>
        
    </div>

</asp:Content>
