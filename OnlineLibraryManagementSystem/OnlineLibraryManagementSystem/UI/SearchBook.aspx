﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="SearchBook.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.SearchBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">

        <div class="panel-heading">
            <div class="h3 text-center">Search a Book</div>
        </div>

        <div class="panel-body">
            <div style="padding-bottom: 10px">
                <asp:Label ID="lblErrorMsg" runat="server" Style="padding-left: 250px" ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Publication: </label>
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlPublication" CssClass="form-control" runat="server" DataTextField="publication" DataValueField="id"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Category: </label>
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlCategory" CssClass="form-control" runat="server" DataTextField="category" DataValueField="id"></asp:DropDownList>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Book Name:</label>
                            </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtBookName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success btn-block btn-lg" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <asp:Repeater ID="rptrSearchBook" runat="server">
                        <HeaderTemplate>
                            <table class="table table-bordered table-responsive table-striped ">
                                <tr style="background-color: #d2b48c; font-size: large; font-weight: bold">
                                    <td>Book Name</td>
                                    <td>Author Name</td>
                                    <td>Publication</td>
                                    <td>Category</td>
                                    <td colspan="3">Actoin</td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Eval("name") %>
                                </td>
                                <td>
                                    <%#Eval("author") %>
                                </td>
                                <td>
                                    <%#Eval("publication") %>
                                </td>
                                <td>
                                    <%#Eval("category") %>
                                </td>
                                <td>
                                    <a href="BookDetail.aspx?DetailId=<%#Eval("id") %>">Details</a>
                                </td>
                                <td>
                                    <a href="EditBook.aspx?EditBookId=<%#Eval("id") %>">Edit</a>
                                </td>
                                <td>
                                    <a href="SearchBook.aspx?DelBookid=<%#Eval("id") %>">Delete</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>


        </div>
    </div>

    

</asp:Content>
