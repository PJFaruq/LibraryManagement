﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class UserLogin : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataTable datatable = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                BLLStudent bllstudent = new BLLStudent();
                DALStudent dalstudent = new DALStudent();


                if (txtEmail.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                    lblErrorMsg.Text = "Please Enter the Email";
                    return;
                }
                if (txtPassword.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                    lblErrorMsg.Text = "Please Enter Password";
                    return;
                }
                bllstudent.Email = txtEmail.Text;
                bllstudent.Password = txtPassword.Text;

                string email = txtEmail.Text;
                string password = txtPassword.Text;

                dt = dalstudent.dalGetStudentMailPassword(bllstudent);
                datatable = dalstudent.GetAdminMailPassword(email, password);


                if (dt.Rows.Count > 0)
                {
                    Session["User"] = "Student";
                    Session["StudentLogin"] = "true";
                    Session["StudentColumnId"] = dt.Rows[0]["id"];
                    Session["StudentId"] = dt.Rows[0]["studentId"];


                    Response.Redirect("Home.aspx");
                    return;
                }

                else if (datatable.Rows.Count > 0)
                {
                    Session["User"] = "Admin";

                    Response.Redirect("Home.aspx");
                    return;
                }


                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                    lblErrorMsg.Text = "Incorrect Username or Password";
                    return;
                }
            }
            catch
            {
                throw;
            }

        }
    }
}