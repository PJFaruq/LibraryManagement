﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool status = false;
                BllBook book = new BllBook();
                dalBook dalBook = new dalBook();
                DataTable dt = new DataTable();

                if (txtCategory.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter Category Name";
                    return;
                }

                book.Category = txtCategory.Text;
                dt = dalBook.GetCategory(book);
                if (dt.Rows.Count > 0)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                    lblErrorMsg.Text = "This Category Name is already Exist";
                    return;
                }

                status = dalBook.AddCategory(book);
                if (status == true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                    lblErrorMsg.Text = "Category Added Successfully........";
                    GridViewCategory.DataBind();
                    txtCategory.Text = "";
                    
                }
            }
            catch
            {
                
            }
        }
    }
}