﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class ViewStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDepartment();
                GetAllStudent();
                string id = Request.QueryString["DelStudentid"];
                if (id != null)
                {
                    DeleteStudent(id);

                }
            }


        }

        private void DeleteStudent(string id)
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            bool st = false;

            st = dalstudent.DeleteStudent(id);

            if (st == true)
            {
                GetAllStudent();
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                lblErrorMsg.Text = "Delete Succesfully....";
            }
            if (st == false)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "Can not be Deleted.....";
            }

        }

        private void GetAllStudent()
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            dt = dalstudent.GetAllStudent();
            if (dt.Rows.Count > 0)
            {
                rptrSearchStudent.DataSource = dt;
                rptrSearchStudent.DataBind();
            }
            else if(dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "No Student Available";
            }
        }

        private void GetDepartment()
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();
            dt = dalstudent.GetAllDepartment();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
            ListItem category = new ListItem(" ", "-1");
            ddlDepartment.Items.Insert(0, category);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BLLStudent bllstudent = new BLLStudent();
            DALStudent dalstudent = new DALStudent();
            DataTable dt = new DataTable();

            bllstudent.Department = ddlDepartment.SelectedItem.Text;
            bllstudent.StudentId = txtStudentId.Text;
            dt = dalstudent.GetStudent(bllstudent);
            if (dt.Rows.Count > 0)
            {
                rptrSearchStudent.Visible = true;
                lblErrorMsg.Visible = false;
                rptrSearchStudent.DataSource = dt;
                rptrSearchStudent.DataBind();
            }
            if (dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Red;
                lblErrorMsg.Text = "No Student is Found";
                rptrSearchStudent.Visible = false;
            }
            txtStudentId.Text = "";

        }
    }
}