﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLogin.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.UserLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Design.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">

        <div class="row">
                <div class="col-md-12">
                    <div class="HeadBorder">

                        <table style="margin:0px auto">
                            <tr>
                                <td style="padding:10px"><img src="/Image/book.png" height="100" width="120" /> </td>
                                
                                <td><h1 class="headerText">DAFFODIL LIBRARY</h1> </td>
                                <td><img src="/Image/bookRead.png" height="100" width="120" /></td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
            </div>
        <div class="row " style="margin-top:20px">
            
            <div class="col-md-4 col-md-offset-4 well">
                <img src="../Image/DIU-Logo.png" height="80" width="280"/>
                <p class="h3">Sign in to Library</p>
                <div class="ErrorMsg">
                <asp:Label ID="lblErrorMsg" runat="server" Visible="false" Font-Italic="true" Font-Bold="true"></asp:Label>
            </div>
                <div >
                    <div class="form-group">
                        <label class="control-label">Email:</label>
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Enter Email" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password:</label>
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" placeholder="Enter Password" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                            <asp:CheckBox ID="checkRemember" runat="server" />
                        <label class="control-label">Remember Me</label>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnLogin" runat="server" Text="Submit" CssClass="btn btn-primary" Width="100%" OnClick="btnLogin_Click"/>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
                <div class="col-md-12">
                    <div class="HeadBorder" style="color:white">
                        <em>
                        Developed By:<br />
                                            <a href="https://www.facebook.com/pj.faruq" target="_blank"  style="color:white">PJ Faruq</a><br />
                                           
                                            Contact: mdfaruqsarker@gmail.com<br />

                                © 2013-2016 All Rights Reserved
                            </em>
                    </div>
                </div>
            </div>
    </div>
    </form>
        <script src="../Scripts/jquery-1.10.2.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
</body>
</html>
