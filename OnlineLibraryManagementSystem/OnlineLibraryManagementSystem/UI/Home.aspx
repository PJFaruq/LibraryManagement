﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Library.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OnlineLibraryManagementSystem.UI.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="h3 text-center">Welcome to Our Library</div>
        </div>
        <div class="panel-body">
            <img src="../Image/library.jpg"  height="400" width="800"/>
        </div>
    </div>
</asp:Content>
