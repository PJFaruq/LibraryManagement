﻿using OnlineLibraryManagementSystem.BLL;
using OnlineLibraryManagementSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineLibraryManagementSystem.UI
{
    public partial class ReturnBookDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["ReturnBookId"];
            if (id == null)
            {
                Response.Redirect("ReturnBook.aspx");
                return;
            }
            if (!IsPostBack)
            {
                GetReturnBookDetail(id);
            }
            
        }

        private void GetReturnBookDetail(string id)
        {
            BLLIssueBook bllIssueBook = new BLLIssueBook();
            DALIssueBook dalIssueBook = new DALIssueBook();
            DataTable dt = new DataTable();
            bllIssueBook.id =Convert.ToInt32( id);

            dt = dalIssueBook.GetReturnBookDetail(bllIssueBook);
            if (dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "No Record is Found";
                return;
            }
            rptrReturnBookDetail.DataSource = dt;
            rptrReturnBookDetail.DataBind();
        }

        protected void btnReturnBook_Click(object sender, EventArgs e)
        {
            BLLIssueBook bllIssueBook = new BLLIssueBook();
            DALIssueBook dalIssueBook = new DALIssueBook();
            DataTable dt = new DataTable();

            bool status = false;
            string id = Request.QueryString["ReturnBookId"];
            bllIssueBook.id = Convert.ToInt32(id);

            dt = dalIssueBook.GetReturnBookDetail(bllIssueBook);

            string bookName = dt.Rows[0]["bookName"].ToString();
            string authorName = dt.Rows[0]["authorName"].ToString();


            status = dalIssueBook.DeleteReturnBook(bllIssueBook);

            if (status == true)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.ForeColor = System.Drawing.Color.Green;
                lblErrorMsg.Text = "This Book is Successfully Returned ";

                BllBook bllBook = new BllBook();
                dalBook dalBook = new dalBook();
                DataTable dt1 = new DataTable();

                dt1 = dalBook.GetBookAndAuthorName(bookName, authorName);
                int available = Convert.ToInt32(dt1.Rows[0]["available"]);
                available++;
                int rent = Convert.ToInt32(dt1.Rows[0]["rent"]);
                rent--;
                bllBook.id = Convert.ToInt32(dt1.Rows[0]["id"]);
                bllBook.Available = available;
                bllBook.Rent = rent;
                dalBook.UpdateAvailable(bllBook);

            }

        }
    }
}